# Technical Test Solution

# Author: Vítor Eulálio Reis


## Access

 This public repo comprises the solutions for the 8 Problems. The solutions have been organized in branches. There are 9 branches additonaly to the master, one per Problem/Question.

 If one wants to clone this repo, in order to visualize the branches one must checkout all branches.

 ```

 vitor@hal9000:~/Projects/testing/instacluster-test$ git branch -a

 * master
 remotes/origin/solution-1

 remotes/origin/solution-2

 remotes/origin/solution-3

 remotes/origin/solution-4

 remotes/origin/solution-5

 remotes/origin/solution-6

 remotes/origin/solution-7

 remotes/origin/solution-8

 remotes/origin/solution-9

 ```

## Time Management

Toggle has been used to track the work throughout the Problems and the report on time management is the time_report.pdf file.
